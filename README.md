# Swapi Dev Integration Test

Projeto de exemplo para implementações de testes automatizados com Java e RestAssured a uma API pública.

End-point alvo dos testes: https://swapi.dev/api/

Visualize as páginas da Wiki deste projeto para consumir este conteúdo:
https://gitlab.com/bruno.lusa/swapi-dev-integration-test/-/wikis/home

