package org.sample.swapi.dev.integration.test.saude;

import org.testng.annotations.Test;

import java.io.File;

import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;

public class HealthCheckTest {

    @Test
    public void validandoSaudeDaAplicacao(){
        baseURI = "https://swapi.dev/";
        basePath = "api/";

        when().
            get("/").
        then().
            statusCode(200);

    }
}
