package org.sample.swapi.dev.integration.test.contrato;

import java.io.File;
import org.testng.annotations.Test;
import static io.restassured.RestAssured.*;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;


public class GetPeopleContractTest {

    @Test
    public void validandoContratoGetPeople(){
        baseURI = "https://swapi.dev/";
        basePath = "api/";

        when().
            get("/people").
        then().
            statusCode(200).
            body(matchesJsonSchema(new File("src/test/resources/jsonSchema/GetAllPeoples.json")));

    }
}
